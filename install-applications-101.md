theme: Zurich, 1

# [fit] InstallApplication**s** 101

Mac Justice 
Client Platform Engineer @ Dropbox

^ I called out the final S, because it makes this conversation sort of confusing

---

# [fit] Imaging is Dead™

^ OK, we got that out of the way

---

# How do you provision Macs starting with only MDM?

---

# InstallApplication

## MDM Command

- Must be a distribution pkg
- Must be signed
- `mdmclient` is surprisingly easy to break if you send it mixed messages!

^ MDM has a command, InstallApplication, which is used to install packages.

^ However, it has some restrictions and liabilities that makes it difficult to develop your deployment process

^ On Sierra you didn't even get acknowledgement if the install succeeded or not!

---

# MDM :arrow_right: Agent

## Vendor Agents

- JAMF
- AirWatch/Workspace One

:arrow_up: Vendors usually prioritize their own agent.

^ Several of the major commercial vendors use InstallApplication to deliver their own agent.

^ It's typical for the vendor to prioritize their own agent, and require/encourage you to use it for further configuration

--- 

# MDM :arrow_right: Agent

## Third Party Agents

- Munki
- Puppet
- Chef
- Etc...

^ However, what if your environment is dependent on other tools? How do you tell MDM to deliver them?

---

# InstallApplication**s**

## A very small bootstrap package that refers to an internet hosted JSON file to dynamically run scripts and install packages during and after Setup Assistant

^ To solve this problem, Erik Gomez, now at Uber, wrote a tool he calls InstallApplications

^ In some ways, IAs is Munki-lite

^ There's a package that launches jobs to read a web hosted manifest file, which then installs packages and runs scripts as described in the manifest.

---

# Components

1. Bootstrap Package
2. JSON manifest and payload

^ There are two parts:

^ The bootstrap package is delivered to the deployed Mac

^ Which then reads the JSON manifest and begins running the packages and scripts listed

---

# Bootstrap Package

- Build using Munki-pkg
- Set JSON URL before building
- Must be signed with Apple Developer's account
- Can optionally launch DEPnotify

^ You download the code for the bootstrap package from Erik's GitHub page (see references)

^ Build it using munki-pkg (a useful tool worth giving a talk on!)

^ First, you edit it to include your JSON's URL

^ You must have an Apple Developer accoun to sign the package

^ You can also use it to launch DEPNotify when the user first logs in

---

# JSON Manifest

- Generate with the included `generatejson.py` script
- Hosted where the deploying Mac can access it

^ JSON is just a form of structured text, not too different from a plist

^ Use the included python script to help generate it, or you can even write it by hand

^ Host it on a web server, S3 bucket, or wherever. It just needs a URL that the machine will be able to resolve. Lots of ways to skin this cat.

^ We currently host ours on an internal server, you can do that or host it publically. InstallApplications can do HTTP basic authentication for some simple security. Please, please use TLS/SSL, especially if you host your JSON publically.

---

# JSON Manifest Sections

1. Preflight - Continue or Exit and Remove IAs
2. Setup Assistant phase
3. Userland phase

^ Preflight stage is used to determine if the computer is already configured, useful when enrolling an already deployed fleet into MDM, and avoiding an accidental re-run of your deployment process

^ Setup Assistant stage happens in the background during Setup Assistant, before a user session has started. Useful for getting the stuff that doesn't need to wait for a user's presence

^ Userland happens in the user session, after Setup Assistant completes. If you have scripts that modify the user's environment, this is a good place to put them.

---

# JSON Manifest Content

```json
 {
    "file": "/Library/Application Support/installapplications/setupassistant.pkg",
    "url": "https://domain.tld/setupassistant.pkg",
    "packageid": "com.package.setupassistant",
    "version": "1.0",
    "hash": "sha256 hash",
    "name": "setupassistant Package Name",
    "type": "package"
}
```

^ An example item entry
Looks a little different from a plist, but you'll recognize the key:value pattern
- File: path to install the pkg from
- Url: where the pkg is hosted
- hash: hash to confirm pkg integrity
- name: name to use in debug output and DEPNotify
- version: for receipt checking
- packageid: also receipts
- type: can be package, rootscript, or userscript

---

# Why am I using InstallApplications?

- Avoiding vendor lock-in
- Existing open-source infrastructure investment
- One fewer agent to attack
- Changing the workflow only requires modifying the JSON, not repackaging or messing with a GUI
- Source control & Continuous Integration

---

# What's in my JSON?

- AD bind :grimacing:
- DEPNotify :signal_strength:
- Puppet :japanese_goblin:
- Munki :monkey:
- Scripts to kickstart the above

---

# Supported MDMs

- MicroMDM
- SimpleMDM
- AirWatch/Workspace One[^1]
- FileWave[^2]
- ZuluDesk[^3]
    
[^1]: Must opt-out of agent install

[^2]: Contact support for instructions

[^3]: According to a [blog](http://cannonball.tombridge.com/2017/04/27/getting-started-with-installapplication-depnotify-and-simplemdm/) footnote?

---

# References

- Source Code on Github: [erikng/installapplications](https://github.com/erikng/installapplications)
- Erik Gomez: ["Custom DEP"](https://blog.eriknicolasgomez.com/2017/03/08/Custom-DEP-Part-1-An-Introduction/) blog post series
- Tom Bridge: ["Getting Started with InstallApplication, DEPNotify and SimpleMDM"](http://cannonball.tombridge.com/2017/04/27/getting-started-with-installapplication-depnotify-and-simplemdm/)
